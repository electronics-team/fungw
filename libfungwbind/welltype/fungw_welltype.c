/*
    fungw - language-agnostic function gateway
    Copyright (C) 2017, 2019, 2021  Tibor 'Igor2' Palinkas

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

    Project page: http://repo.hu/projects/fungw
    Version control: svn://repo.hu/fungw/trunk
*/

#include <stdlib.h>
#include <libfungw/fungw.h>
#include <libfungw/fungw_conv.h>
#include <wt.h>
#include <lauxlib.h>
#include <wtlib.h>


/* Convert fgw type to wt type and push */
static void fgw_wt_push(fgw_ctx_t *fctx, wt_State *lst, fgw_arg_t *arg)
{
#	define FGW_wt_PUSH_LONG(lst, val)   wt_pushinteger(lst, val); return;
#	define FGW_wt_PUSH_DOUBLE(lst, val) wt_pushnumber(lst, val); return;
#	define FGW_wt_PUSH_PTR(lst, val)    wt_pushlightuserdata(lst, val); return;
#	define FGW_wt_PUSH_STR(lst, val)    wt_pushstring(lst, val); return;
#	define FGW_wt_PUSH_NIL(lst, val)    wt_pushnil(lst); return;

	if (FGW_IS_TYPE_CUSTOM(arg->type))
		fgw_arg_conv(fctx, arg, FGW_AUTO);

	switch(FGW_BASE_TYPE(arg->type)) {
		ARG_CONV_CASE_LONG(lst, FGW_wt_PUSH_LONG);
		ARG_CONV_CASE_LLONG(lst, FGW_wt_PUSH_DOUBLE);
		ARG_CONV_CASE_DOUBLE(lst, FGW_wt_PUSH_DOUBLE);
		ARG_CONV_CASE_LDOUBLE(lst, FGW_wt_PUSH_DOUBLE);
		ARG_CONV_CASE_PTR(lst, FGW_wt_PUSH_PTR);
		ARG_CONV_CASE_STR(lst, FGW_wt_PUSH_STR);
		ARG_CONV_CASE_CLASS(lst, FGW_wt_PUSH_NIL);
		ARG_CONV_CASE_INVALID(lst, FGW_wt_PUSH_NIL);
	}
	if (arg->type & FGW_PTR) {
		FGW_wt_PUSH_PTR(lst, arg->val.ptr_void);
	}
	else {
		FGW_wt_PUSH_NIL(lst, 0);
	}
}

/* Read the wt stack and convert the result to an fgw arg */
static void fgw_wt_toarg(wt_State *lst, fgw_arg_t *dst, int n)
{
	switch(wt_type(lst, n)) {
		case wt_TNUMBER:
			dst->type = FGW_DOUBLE;
			dst->val.nat_double = wt_tonumber(lst, n);
			break;
		case wt_TBOOLEAN:
			dst->type = FGW_INT;
			dst->val.nat_int = !!wt_toboolean(lst, n);
			break;
		case wt_TSTRING:
			dst->type = FGW_STR | FGW_DYN;
			dst->val.str = fgw_strdup(wt_tostring(lst, n));
			break;
		case wt_TLIGHTUSERDATA:
			dst->type = FGW_PTR;
			dst->val.ptr_void = wt_touserdata(lst, n);
			break;
		case wt_TNIL:
		default:
			dst->type = FGW_PTR;
			dst->val.ptr_void = NULL;
			break;
	}
}

/* API: the script is calling an fgw function */
static int fgws_wt_call_fgw(wt_State *lst)
{
	fgw_obj_t *obj;
	int argc, n;
	fgw_arg_t res, *argv, argv_static[16];
	fgw_func_t *func;
	fgw_error_t err;
	wt_Debug ar;

	/* Get the current function-name */
	wt_getstack(lst, 0, &ar);
	wt_getinfo(lst, "n", &ar);

	obj = fgws_wt_get_obj(lst);

	func = fgw_func_lookup(obj->parent, ar.name);
	if (func == NULL)
		return 0;

	argc = wt_gettop(lst);

	if ((argc + 1) > (sizeof(argv_static) / sizeof(argv_static[0])))
		argv = malloc((argc + 1) * sizeof(fgw_arg_t));
	else
		argv = argv_static;

	argv[0].type = FGW_FUNC;
	argv[0].val.argv0.func = func;
	argv[0].val.argv0.user_call_ctx = obj->script_user_call_ctx;

	for (n = 1; n < argc; n++)
		fgw_wt_toarg(lst, &argv[n], n);

	/* Run command */
	res.type = FGW_PTR;
	res.val.ptr_void = NULL;
	err = func->func(&res, argc, argv);

	/* Free the array */
	fgw_argv_free(obj->parent, argc, argv);
	if (argv != argv_static)
		free(argv);

	if (err != 0)
		return 0;

	fgw_wt_push(obj->parent, lst, &res);

	if (res.type & FGW_DYN)
		free(res.val.ptr_void);

	return 1;
}

/* API: register an fgw function in the script, make the function visible/callable */
static void fgws_wt_reg_func(fgw_obj_t *obj, const char *name, fgw_func_t *f)
{
	wt_State *lst = obj->script_data;
	wt_register(lst, name, fgws_wt_call_fgw);
}

/* API: fgw calls a wt function */
static fgw_error_t fgws_wt_call_script(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	fgw_obj_t *obj = argv[0].val.func->obj;
	wt_State *lst = obj->script_data;
	int i;

	wt_getglobal(lst, argv[0].val.func->name);

	for (i = 1; i < argc; i++)
		fgw_wt_push(obj->parent, lst, &argv[i]);

	fgws_ucc_save(obj);
	wt_call(lst, argc-1, 1);
	fgws_ucc_restore(obj);

	fgw_wt_toarg(lst, res, 1);
	wt_pop(lst, 1);
	return FGW_SUCCESS;
}


/* API: unload the script */
static int fgws_wt_unload(fgw_obj_t *obj)
{
	if (obj->script_data != NULL)
		wt_close(obj->script_data);
	obj->script_data = NULL;
	return 0;
}

/* Helper function for the script to register its functions */
static int fgws_wt_freg(wt_State *lst)
{
	fgw_obj_t *obj;
	const char *name;
	int argc;
	fgw_func_t *func;

	obj = fgws_wt_get_obj(lst);

	argc = wt_gettop(lst);
	if (argc != 2) {
		fgw_async_error(obj, "fgw_func_reg: wrong number of arguments: need 2\n");
		return 0;
	}

	name = wt_tostring(lst, 1);
	if (name == NULL) {
		fgw_async_error(obj, "fgw_func_reg: empty name\n");
		return 0;
	}

	func = fgw_func_reg(obj, name, fgws_wt_call_script);
	if (func == NULL) {
		fgw_async_error(obj, "fgw_func_reg: failed to register function\n");
		fgw_async_error(obj, name);
		fgw_async_error(obj, "\n");
		return 0;
	}

	return 1;
}

/* API: init the interpreter so that functions can be registered */
static int fgws_wt_init(fgw_obj_t *obj, const char *filename, const char *opts)
{
	wt_State *lst;

	/* initialize the interpreter */
	obj->script_data = lst = wtL_newstate();

	if (lst == NULL) {
		fgw_async_error(obj, "fgws_wt_init: failed to set up the interpreter\n");
		return -1;
	}

	/* Load default libs */
#if wt_VERSION_NUM < 500
	wtopen_base(lst);
	wtopen_io(lst);
	wtopen_string(lst);
	wtopen_math(lst);
#else
	wtL_openlibs(lst);
#endif

	/* add the wt->fgw glue */
	wt_register(lst, "fgw_func_reg", fgws_wt_freg);
	wt_pushlightuserdata(lst, obj);
	wt_setglobal(lst, wt_OBJ_NAME);
	return 0;
}

/* API: load a script into an object */
static int fgws_wt_load(fgw_obj_t *obj, const char *filename, const char *opts)
{
	wt_State *lst = obj->script_data;
	int res;

	/* Load the file */
	res = wtL_loadfile(lst, filename);

	if (res != 0) {
		fgw_async_error(obj, "fgws_wt_load: failed to load the script\n");
		wt_close(obj->script_data);
		obj->script_data = NULL;
		return -1;
	}

	/* Run the main part */
	if (wt_pcall(lst, 0, 0, 0) != 0) {
		fgw_async_error(obj, "fgws_wt_load: failed to execute the script\n");
		wt_close(obj->script_data);
		obj->script_data = NULL;
		return -2;
	}

	return 0;
}

static int fgws_wt_test_parse(const char *filename, FILE *f)
{
	const char *exts[] = {".wt", NULL };
	return fgw_test_parse_fn(filename, exts);
}

/* API: engine registration */
static const fgw_eng_t fgw_wt_eng = {
	"wt",
	fgws_wt_call_script,
	fgws_wt_init,
	fgws_wt_load,
	fgws_wt_unload,
	fgws_wt_reg_func,
	NULL,
	fgws_wt_test_parse,
	".wt"
};

int pplg_check_ver_fungw_wt(int version_we_need)
{
	return 0;
}

int pplg_init_fungw_wt(void)
{
	fgw_eng_reg(&fgw_wt_eng);
	return 0;
}

void pplg_uninit_fungw_wt(void)
{
	fgw_eng_unreg(fgw_wt_eng.name);
}
