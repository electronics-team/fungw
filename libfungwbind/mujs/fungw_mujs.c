/*
    fungw - language-agnostic function gateway
    Copyright (C) 2021  Tibor 'Igor2' Palinkas

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

    Project page: http://repo.hu/projects/fungw
    Version control: svn://repo.hu/fungw/trunk
*/

#include <stdlib.h>
#include <string.h>
#include <libfungw/fungw.h>
#include <libfungw/fungw_conv.h>
#include <mujs.h>

#define FGW_FUNC_PROP "__fungw_function_name__"

static void fgws_mujs_print(js_State *ctx)
{
	int n, argc = js_gettop(ctx);
	for(n = 1; n < argc; n++) {
		if (n > 1)
			putchar(' ');
		printf("%s", js_tostring(ctx, n));
	}
	putchar('\n');
}

static void fgws_mujs_push_arg(fgw_ctx_t *fctx, js_State *ctx, fgw_arg_t *arg)
{
	char tmp[128];

#	define FGW_mujs_PUSH_DOUBLE(lst, val)     js_pushnumber(ctx, val); return;
#	define FGW_mujs_PUSH_STR(lst, val)        js_pushstring(ctx, val); return;
#	define FGW_mujs_PUSH_NIL(lst, val)        js_pushnull(ctx); return;
#	define FGW_mujs_PUSH_PTR(lst, val)        sprintf(tmp, "%p", val); js_pushstring(ctx, tmp); return;

	if (FGW_IS_TYPE_CUSTOM(arg->type))
		fgw_arg_conv(fctx, arg, FGW_AUTO); /* if fails, it remains custom and will be unhandled */

	switch(FGW_BASE_TYPE(arg->type)) {
		ARG_CONV_CASE_LONG(NULL, FGW_mujs_PUSH_DOUBLE);
		ARG_CONV_CASE_LLONG(NULL, FGW_mujs_PUSH_DOUBLE);
		ARG_CONV_CASE_DOUBLE(NULL, FGW_mujs_PUSH_DOUBLE);
		ARG_CONV_CASE_LDOUBLE(NULL, FGW_mujs_PUSH_DOUBLE);
		ARG_CONV_CASE_PTR(NULL, FGW_mujs_PUSH_PTR);
		ARG_CONV_CASE_STR(NULL, FGW_mujs_PUSH_STR);
		ARG_CONV_CASE_CLASS(NULL, FGW_mujs_PUSH_NIL);
		ARG_CONV_CASE_INVALID(NULL, FGW_mujs_PUSH_NIL);
	}

	js_pushnull(ctx);
}

static void fgws_mujs_js2arg(js_State *ctx, fgw_arg_t *dst, int src_idx)
{
	int type = js_type(ctx, src_idx);

	switch(type) {
		case JS_ISBOOLEAN:
			dst->type = FGW_INT;
			dst->val.nat_int = js_toboolean(ctx, src_idx);
			break;
		case JS_ISNUMBER:
			dst->type = FGW_DOUBLE;
			dst->val.nat_double = js_tonumber(ctx, src_idx);
			break;
		case JS_ISSTRING:
			dst->type = FGW_STR | FGW_DYN;
			dst->val.str = fgw_strdup(js_tostring(ctx, src_idx));
			break;
		case JS_ISNULL:
			dst->type = FGW_PTR;
			dst->val.ptr_void = NULL;
			break;

		case JS_ISUNDEFINED:
		case JS_ISFUNCTION:
		case JS_ISOBJECT:
			/* can't convert these */
			fprintf(stderr, "fgws_mujs_js2arg: ignoring unconvertable type %d\n", type);
	}
}

/* API: the script is calling an fgw function */
static void fgws_mujs_call_fgw(js_State *ctx)
{
	fgw_func_t *f;
	int n, argc;
	fgw_arg_t res, *argv, argv_static[16];
	fgw_error_t err;
	fgw_obj_t *obj = js_getcontext(ctx);
	const char *fname;

	/* figure the fgw function to call */
	js_currentfunction(ctx);
	js_getproperty(ctx, -1, FGW_FUNC_PROP);
	fname = js_tostring(ctx, -1);
	f = fgw_func_lookup(obj->parent, fname);
	js_pop(ctx, 1);


	argc = js_gettop(ctx);
	if ((argc - 1) > (sizeof(argv_static) / sizeof(argv_static[0])))
		argv = malloc((argc - 1) * sizeof(fgw_arg_t));
	else
		argv = argv_static;

	/* Set the first param */
	argv[0].type = FGW_FUNC;
	argv[0].val.argv0.func = f;
	argv[0].val.argv0.user_call_ctx = obj->script_user_call_ctx;

	for(n = 2; n < argc; n++)
		fgws_mujs_js2arg(ctx, &argv[n-1], -n);

	/* Call the target function */
	res.type = FGW_PTR;
	res.val.ptr_void = NULL;
	err = f->func(&res, argc-1, argv);

	for(n = 2; n < argc; n++)
		fgw_arg_free(obj->parent, &argv[n-1]);

	/* Free the array */
	fgw_argv_free(f->obj->parent, argc, argv);
	if (argv != argv_static)
		free(argv);

	if (err != 0) {
		js_pushnull(ctx);
		return;
	}

	if ((res.type == FGW_PTR) && (res.val.ptr_void == NULL)) {
		js_pushnull(ctx);
		return;
	}

	fgws_mujs_push_arg(f->obj->parent, ctx, &res);

	if (res.type & FGW_DYN)
		free(res.val.ptr_void);
}

static int fgws_mujs_freg_in_script(js_State *ctx, const char *name, void (*f)(js_State *ctx), void *fgw_func)
{
	js_newcfunction(ctx, f, name, 1);
	js_pushstring(ctx, name);
	js_defproperty(ctx, -2, FGW_FUNC_PROP, JS_READONLY | JS_DONTENUM | JS_DONTCONF);
	js_setglobal(ctx, name);
	return 0;
}

/* API: fgw calls a mujs function */
static fgw_error_t fgws_mujs_call_script(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	fgw_obj_t *obj = argv[0].val.func->obj;
	js_State *ctx = obj->script_data;
	int n;

	js_getglobal(ctx, argv[0].val.func->name);
	js_pushnull(ctx);
	for(n = 1; n < argc; n++)
		fgws_mujs_push_arg(obj->parent, ctx, &argv[n]);

	fgws_ucc_save(obj);
	js_pcall(ctx, argc-1);
	fgws_ucc_restore(obj);

	fgws_mujs_js2arg(ctx, res, -1);

	js_pop(ctx, 1); /* result */
	return 0;
}

/* Helper function for the script to register its functions */
static void fgws_mujs_freg_in_fungw(js_State *ctx)
{
	const char *fn;
	int argc = js_gettop(ctx);
	fgw_obj_t *obj = js_getcontext(ctx);
	fgw_func_t *func;

	if (argc != 2) {
		fprintf(stderr, "fgw_func_reg: called with wrong number of arguments (must be 1)\n");
		goto error;
	}

	fn = js_tostring(ctx, -1);
	js_getglobal(ctx, fn);

	if (js_type(ctx, -1) != JS_ISFUNCTION) {
		fgw_async_error(obj, "fgw_func_reg: global function does not exist:");
		fgw_async_error(obj, fn);
		fgw_async_error(obj, "\n");
		goto error;
	}
	

	func = fgw_func_reg(obj, fn, fgws_mujs_call_script);
	if (func == NULL) {
		fgw_async_error(obj, "fgw_func_reg: failed to register function\n");
		fgw_async_error(obj, fn);
		fgw_async_error(obj, "\n");
		goto error;
	}

	js_pushboolean(ctx, 1);
	return;

	error:;
	js_pushboolean(ctx, 0);
}

/* API: register an fgw function in the script, make the function visible/callable */
static void fgws_mujs_reg_func(fgw_obj_t *obj, const char *name, fgw_func_t *f)
{
	fgws_mujs_freg_in_script(obj->script_data, name, fgws_mujs_call_fgw, f);
}


/* API: unload the script */
static int fgws_mujs_unload(fgw_obj_t *obj)
{
	js_State *ctx = obj->script_data;
	js_freestate(ctx);
	return 0;
}

static void fgws_mujs_report(js_State *ctx, const char *message)
{
	fgw_obj_t *obj = js_getcontext(ctx);
	fgw_async_error(obj, "mujs error:");
	fgw_async_error(obj, message);
	fgw_async_error(obj, "\n");
}

/* API: init the interpreter so that functions can be registered */
static int fgws_mujs_init(fgw_obj_t *obj, const char *filename, const char *opts)
{
	js_State *ctx = js_newstate(NULL, NULL, JS_STRICT);
	if (ctx == NULL)
		return -1;
	obj->script_data = ctx;

	js_setreport(ctx, fgws_mujs_report);

	/* add the mujs->fgw glue */
	fgws_mujs_freg_in_script(ctx, "print", fgws_mujs_print, "<fungw:print>");
	fgws_mujs_freg_in_script(ctx, "fgw_func_reg", fgws_mujs_freg_in_fungw, "<fungw:fgw_func_reg>");

	js_setcontext(ctx, obj);

	return 0;
}

/* API: load a script into an object */
static int fgws_mujs_load(fgw_obj_t *obj, const char *filename, const char *opts)
{
	js_State *ctx = obj->script_data;

	if (js_try(ctx)) {
		fgw_async_error(obj, "mujs script load error:");
		fgw_async_error(obj, js_tostring(ctx, -1));
		fgw_async_error(obj, "\n");
		js_pop(ctx, 1);
		return -1;
	}

	js_ploadfile(ctx, filename);
	js_call(ctx, -1);
	js_pop(ctx, 1);

	js_endtry(ctx);

	return 0;
}

static int fgws_mujs_test_parse(const char *filename, FILE *f)
{
	const char *exts[] = {".js", NULL };
	return fgw_test_parse_fn(filename, exts);
}

/* API: engine registration */
static const fgw_eng_t fgw_mujs_eng = {
	"mujs",
	fgws_mujs_call_script,
	fgws_mujs_init,
	fgws_mujs_load,
	fgws_mujs_unload,
	fgws_mujs_reg_func,
	NULL,
	fgws_mujs_test_parse,
	".js"
};

int pplg_check_ver_fungw_mujs(int version_we_need)
{
	return 0;
}

int pplg_init_fungw_mujs(void)
{
	fgw_eng_reg(&fgw_mujs_eng);
	return 0;
}

void pplg_uninit_fungw_mujs(void)
{
	fgw_eng_unreg(fgw_mujs_eng.name);
}
