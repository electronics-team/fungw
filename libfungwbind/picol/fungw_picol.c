/*
    fungw - language-agnostic function gateway
    Copyright (C) 2017, 2019, 2021  Tibor 'Igor2' Palinkas

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Lesser General Public
    License as published by the Free Software Foundation; either
    version 2.1 of the License, or (at your option) any later version.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public
    License along with this library; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA

    Project page: http://repo.hu/projects/fungw
    Version control: svn://repo.hu/fungw/trunk
*/

#include <stdlib.h>
#include <string.h>
#include <libfungw/fungw.h>
#include <libfungw/fungw_conv.h>

#define PICOL_IMPLEMENTATION
#include <picol.h>

static int fgws_picol_test_parse(const char *filename, FILE *f)
{
	const char *exts[] = {".pcl", NULL};
	return fgw_test_parse_fn(filename, exts);
}

static void fgws_picol_err(fgw_obj_t *obj)
{
	picolVar *v = picolGetVar(obj->script_data, "::errorInfo");
	fgw_async_error(obj, v == NULL ? "<unknown picol error>" : v->val);
}

/* API: the script is calling an fgw function */
picolResult fgws_picol_call_fgw(picolInterp *interp, int argc, const char *argv[], void *pd)
{
	fgw_obj_t *obj = pd;
	fgw_arg_t res, *sarg, sarg_static[128];
	int n;
	fgw_func_t *func;
	fgw_error_t err;

	func = fgw_func_lookup(obj->parent, argv[0]);
	if (func == NULL)
		return 0;

	/* alloc arguments */
	if ((argc+1) > (sizeof(sarg_static) / sizeof(sarg_static[0])))
		sarg = malloc((argc+1) * sizeof(fgw_arg_t *));
	else
		sarg = sarg_static;

	/* set up arguments */
	sarg[0].type = FGW_FUNC;
	sarg[0].val.argv0.func = func;
	sarg[0].val.argv0.user_call_ctx = obj->script_user_call_ctx;

	for(n = 1; n < argc; n++) {
		sarg[n].type = FGW_STR;
		sarg[n].val.str = argv[n];
	}

	/* Run command */
	res.type = FGW_PTR;
	res.val.ptr_void = NULL;
	err = func->func(&res, argc, sarg);
	/* no need to free argv - all static strings */

	if (sarg != sarg_static)
		free(sarg);


	if (err == 0) {
		fgw_arg_conv(obj->parent, &res, FGW_STR | FGW_DYN);
		picolSetResult(obj->script_data, res.val.str);
		free(res.val.str);
	}

	return err ? -1 : 0;
}

/* API: register an fgw function in the script, make the function visible/callable */
static void fgws_picol_reg_func(fgw_obj_t *obj, const char *name, fgw_func_t *f)
{
	picolRegisterCmd(obj->script_data, name, fgws_picol_call_fgw, obj);
}

/* API: fgw calls a picol function */
static fgw_error_t fgws_picol_call_script(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	fgw_obj_t *obj = argv[0].val.func->obj;
	picolInterp *interp = obj->script_data;
	int n, evr;
	long len = 0;
	fgw_error_t ret = FGW_SUCCESS;
	const char *i, **sarg, *sarg_static[128];
	char *cmd, *o;

	if ((argc+1) > (sizeof(sarg_static) / sizeof(sarg_static[0])))
		sarg = malloc((argc+1) * sizeof(char *));
	else
		sarg = sarg_static;

	sarg[0] = argv[0].val.func->name;
	for(n = 1; n < argc; n++) {
		fgw_arg_conv(obj->parent, &argv[n], FGW_STR);
		sarg[n] = argv[n].val.str;
		len += strlen(sarg[n])*2 + 3;
	}
	cmd = o = malloc(len+4);
	if (cmd == NULL) {
		if (sarg != sarg_static)
			free(sarg);
		return FGW_ERR_UNKNOWN;
	}

	for(n = 0; n < argc; n++) {
		if (n > 0) *o++ = '"';
		for(i = sarg[n]; *i != '\0'; i++) {
			if ((n > 0) && (*i == '"')) *o++ = '\\';
			*o++ = *i;
		}
		if (n > 0) *o++ = '"';
		*o++ = ' ';
	}
	*o = '\0';

	fgws_ucc_save(obj);
	evr = picolEval(interp, cmd);
	fgws_ucc_restore(obj);
	free(cmd);

	if (evr != PICOL_OK) {
		fgws_picol_err(obj);
		ret = FGW_ERR_UNKNOWN;
	}

	res->type = FGW_STR;
	res->val.str = interp->result;

	fgw_argv_free(obj->parent, argc, argv);
	if (sarg != sarg_static)
		free(sarg);

	return ret;
}

/* API: unload the script */
static int fgws_picol_unload(fgw_obj_t *obj)
{
	picolFreeInterp(obj->script_data);
	obj->script_data = NULL;
	return 0;
}

/* Helper function for the script to register its functions */
picolResult fgws_picol_freg(picolInterp *interp, int argc, const char *argv[], void *pd)
{
	fgw_obj_t *obj = pd;
	fgw_func_t *func;

	if (argc != 2) {
		fgw_async_error(obj, "fgw_func_reg: wrong number of arguments: need 1\n");
		return -1;
	}

	func = fgw_func_reg(obj, argv[1], fgws_picol_call_script);
	if (func == NULL) {
		fgw_async_error(obj, "fgw_func_reg: failed to register function\n");
		fgw_async_error(obj, argv[1]);
		fgw_async_error(obj, "\n");
		return -1;
	}

	return 0;
}

/* API: init the interpreter so that functions can be registered */
static int fgws_picol_init(fgw_obj_t *obj, const char *filename, const char *opts)
{
	obj->script_data = picolCreateInterp();
	picolRegisterCmd(obj->script_data, "fgw_func_reg", fgws_picol_freg, obj);
	return 0;
}

/* API: load a script into an object */
static int fgws_picol_load(fgw_obj_t *obj, const char *filename, const char *opts)
{
	/* Read the file */
	if (picolSource(obj->script_data, filename) == PICOL_OK)
		return 0;

	fgw_async_error(obj, "fgws_picol_load: failed to eval the file\n");
	fgws_picol_err(obj);
	return -1;
}

/* API: engine registration */
static const fgw_eng_t fgw_picol_eng = {
	"picol",
	fgws_picol_call_script,
	fgws_picol_init,
	fgws_picol_load,
	fgws_picol_unload,
	fgws_picol_reg_func,
	NULL,
	fgws_picol_test_parse,
	".pcl"
};

int pplg_check_ver_fungw_picol(int version_we_need)
{
	return 0;
}

int pplg_init_fungw_picol(void)
{
	fgw_eng_reg(&fgw_picol_eng);
	return 0;
}

void pplg_uninit_fungw_picol(void)
{
	fgw_eng_unreg(fgw_picol_eng.name);
}
