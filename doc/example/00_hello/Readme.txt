The same example script written in each supported scripting language. Useful
for example apps in other examples.

The script has an init part that makes a call to atoi(), which should
be an external function (provided by the a host app or another engine
loaded by the host app).

The script then registers a function called 'hello'. When hello() is called,
it prints its two arguments, making one more call to atoi() and returns 66.


