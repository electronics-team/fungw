Script embedding example: register a function scripts can call,
load a script, run the init part of the script then call a function of
the script.

Script is loaded from ../00_hello/. By default the example uses fawk
because that is available without external dependencies. To change the
language, modify the three defines SCLANG, SCLANG_STR and SCRIPT_FN 
at the top of test_script.c.

Build considerations: this example uses static linking, which is not
recommended for production code. $(FUNGWBIND_SRCLIBA) contains all
static linkable script languages.

Linking them all is suboptimal for a real application because:
 - it hardwires language support to whatever is available at the time the
   app is compiled
 - it brings in a lot of unnecessary dependencies, e.g. if you use lua only
   but you had python available at app compilation time, every time you
   start the app it will dynamic link python as well

We use this suboptimal method here because it's just a quick example and
this makes it easier to change scripting languages while keeping the build
simple. There are other examples further down that demonstrate different
build/link strategies.
