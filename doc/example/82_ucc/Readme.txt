Example on using the User Call Context: the app can create a context
struct at the initial call into fungw and the pointer of that context
is passed through the call stack so when an engine or a function of
the app is called back it can look at the context.

A typical use case is when the app is dealing with multiple independent
set of data, calling a script while working on a specific one of those. Then
the script calls back an utility function of the app that has to touch the
data in the right data set. With UCC the app can place an opaque context
pointer that identifies the data set in question and at any call from the
script this context pointer is available.

Implementation details: the UCC is stored in argv[0] and script bindings
guarantee to preserve it through further function calls.

