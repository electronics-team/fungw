#include <stdlib.h>
#include <stdio.h>
#include <libfungw/fungw.h>

/* User configuration for this example: change language and script file name */
#define SCLANG      fawk
#define SCLANG_STR  "fawk"
#define SCRIPT_FN   "../00_hello/hello.fawk"

/* Macro wizardy for making the example language-independent */
#define SCLANG_INIT_(LANG) pplg_init_fungw_ ## LANG ()
#define SCLANG_INIT(LANG) SCLANG_INIT_(LANG)
extern int SCLANG_INIT(SCLANG);

/* Handle runtime fungw errors - inform the user */
void async_error(fgw_obj_t *obj, const char *msg)
{
	printf("Async error: %s: %s\n", obj->name, msg);
}

/* Script calls C function: wrapper for atoi() */
fgw_error_t fgwc_atoi(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	char *end;
	int ten = 10, *base = FGW_USER_CALL_CTX;
	FGW_DECL_CTX;                        /* setup for the FGW macro shorthands, initializes fgw_ctx_t *ctx */
	FGW_ARGC_REQ_MATCH(1);               /* require exactly one argument */
	FGW_ARG_CONV(&argv[1], FGW_STR);     /* make sure argv[1] is a string */

	/* call the function, prepare the result in res */
	if (base == NULL) base = &ten;       /* this happens in the init part of the script - since that's not a function call from the app, there's no user call context */
	res->val.nat_int = strtol(argv[1].val.str, &end, *base);
	res->type = FGW_INT;
	return *end == '\0' ? FGW_SUCCESS : FGW_ERR_ARG_CONV;
}

int main(int argc, char *argv[])
{
	fgw_ctx_t ctx;
	fgw_obj_t *hobj, *sobj;
	fgw_arg_t res;
	int rv, base = 16;

	if (argc > 1)
		base =atoi(argv[1]);

	if ((base < 2) || (base > 36)) {
		fprintf(stderr, "ERROR: invalid base '%s': must be between 2 and 36\n", argv[1]);
		return 1;
	}

	/* Initialize the language engine; in a more advanced setup the .so is
	   loaded using dlopen() which automates this step; in a static link
	   setup, like this example, the script lang engine needs to be linked and
	   initialized manually. */
	SCLANG_INIT(SCLANG);

	/* initialize a context */
	fgw_init(&ctx, "my_context");
	ctx.async_error = async_error;

	/* create a host object and register our own functions there */
	hobj = fgw_obj_reg(&ctx, "host_app");
	fgw_func_reg(hobj, "atoi", fgwc_atoi);

	/* create objects (load script) */
	sobj = fgw_obj_new(&ctx, "hello", SCLANG_STR, SCRIPT_FN, NULL);
	if (sobj == NULL) {
		fprintf(stderr, "ERROR: failed to load script %s (language %s)\n", SCRIPT_FN, SCLANG_STR);
		return 1;
	}

	/* Call a script function; pass a pointer to base for the UCC */
	printf("\n### Call hello()\n");
	rv = fgw_uvcall(&ctx, &base, &res, "hello",   FGW_INT, (int)12,   FGW_STR, "blobbs",   0);
	if (rv == 0) {
		int cr = fgw_arg_conv(&ctx, &res, FGW_INT); /* convert result to int if it is not an int already */
		if (cr == 0)
			printf("hello() result=%d\n", res.val.nat_int);
		else
			printf("hello() result=<conversion failure>\n");
	}
	else
		fprintf(stderr, "ERROR: calling hello() failed\n");


	fgw_uninit(&ctx);   /* uninit the context; new contexts could still be created */
	fgw_atexit();       /* free all memory used by the lib */

	return 0;
}
