#!/bin/sh

first_para() {
	awk '
		/^[ \t]*$/ { exit }
		{ print $0 }
	'
}

gen_index()
{

cat index_pre.html
echo "EXAMPLES=\\" >&3

for n in *
do
	if test -f $n/Readme.txt
	then
		echo "<tr><td> <a href=\"$n\"> $n </a> <td><p>"
		first_para < $n/Readme.txt
		echo "	$n \\" >&3
	fi
done

cat index_post.html
echo "" >&3

}


gen_index > index.html 3>Makefile.ex
