Demonstrate app build option: script plugins dynamic linked with -l's.
Otherwise does the same as 10_script (loads and runs a script calling
function forth and back).

This example is about the same as static linking: the set of script
languages available runtime is determined compile time. The main drawback
compared to puplug or dlopen is that if a new fungw binding .so is
installed later, the app will not use it unless the app is recompiled
with a new -l.

This build option is normally not recommended outside of bootstrapping
a project or experimenting with the code.

