Demonstrate app build option: dynamic script plugin loading with dlopen().
Otherwise does the same as 10_script (loads and runs a script calling
function forth and back).

The advantage of this setup is that it loads only that script language
plugin (.so) that is required for running the script, which is specified
run-time. This means less dependencies are dynamic linked on app startup.
The drawback not using puplug is more boilerplate around dlopen and
potential portability problems on exotic targets where dlopen may work
differently.




