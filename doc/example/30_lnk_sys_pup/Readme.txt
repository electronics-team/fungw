Demonstrate app build option: dynamic script plugin loading with puplug.
Otherwise does the same as 10_script (loads and runs a script calling
function forth and back).

This example requires puplug installed on the system. Puplug can be
downloaded from:

svn://svn.repo.hu/puplug/trunk

The advantage of this setup is that it loads only that script language
plugin (.so) that is required for running the script, which is specified
run-time. This means less dependencies are dynamic linked on app startup.

(Note: puplug loads the required fungw language binding .so plugin, e.g.
fungw_lua.so using dlopen(); then fungw_lua.so depends on system installed
liblua which is recursively loaded by dlopen.)




