#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <puplug/puplug.h>
#include <libfungw/fungw.h>

/* Handle runtime fungw errors - inform the user */
void async_error(fgw_obj_t *obj, const char *msg)
{
	printf("Async error: %s: %s\n", obj->name, msg);
}

/* Script calls C function: wrapper for atoi() */
fgw_error_t fgwc_atoi(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	FGW_DECL_CTX;                        /* setup for the FGW macro shorthands, initializes fgw_ctx_t *ctx */
	FGW_ARGC_REQ_MATCH(1);               /* require exactly one argument */
	FGW_ARG_CONV(&argv[1], FGW_STR);     /* make sure argv[1] is a string */

	/* call the function, prepare the result in res */
	res->val.nat_int = atoi(argv[1].val.str);
	res->type = FGW_INT;
	return FGW_SUCCESS;
}

/* concat two strings into a newly allocated string */
static char *str_concat2(const char *s1, const char *s2)
{
	long l1 = strlen(s1), l2 = strlen(s2);
	char *res = malloc(l1+l2+1);

	assert(res != NULL);

	memcpy(res,    s1, l1);
	memcpy(res+l1, s2, l2+1);

	return res;
}

int main(int argc, char *argv[])
{
	pup_context_t plugins;
	pup_plugin_t *script_plugin;
	const char *plugin_dirs[] = { PREFIX "/puplug", NULL };
	const char *lang_name = SCLANG_STR;
	const char *script_fn = SCRIPT_FN;
	char *plugin_name;
	fgw_ctx_t ctx;
	fgw_obj_t *hobj, *sobj;
	fgw_arg_t res;
	int rv;

	pup_init(&plugins);

	if (argc == 3) {
		lang_name = argv[1];
		script_fn = argv[2];
	}
	else if (argc != 1) {
		fprintf(stderr, "ERROR: wrong number of arguments; must be %s lang script\n", argv[0]);
		return 1;
	}

	rv = 0;
	plugin_name = str_concat2("fungw_", lang_name);
	script_plugin = pup_load(&plugins, plugin_dirs, plugin_name, 0, &rv);
	if (script_plugin == NULL) {
		fprintf(stderr, "ERROR: failed to load puplug plugin %s for script language %s\n", lang_name, plugin_name);
		free(plugin_name);
		return 1;
	}
	free(plugin_name);

	/* initialize a context */
	fgw_init(&ctx, "my_context");
	ctx.async_error = async_error;

	/* create a host object and register our own functions there */
	hobj = fgw_obj_reg(&ctx, "host_app");
	fgw_func_reg(hobj, "atoi", fgwc_atoi);

	/* create objects (load script) */
	sobj = fgw_obj_new(&ctx, "hello", lang_name, script_fn, NULL);
	if (sobj == NULL) {
		fprintf(stderr, "ERROR: failed to load script %s (language %s)\n", SCRIPT_FN, SCLANG_STR);
		return 1;
	}

	/* Call a script function */
	printf("\n### Call hello()\n");
	rv = fgw_vcall(&ctx, &res, "hello",   FGW_INT, (int)12,   FGW_STR, "blobbs",   0);
	if (rv == 0) {
		int cr = fgw_arg_conv(&ctx, &res, FGW_INT); /* convert result to int if it is not an int already */
		if (cr == 0)
			printf("hello() result=%d\n", res.val.nat_int);
		else
			printf("hello() result=<conversion failure>\n");
	}
	else
		fprintf(stderr, "ERROR: calling hello() failed\n");

	fgw_uninit(&ctx);     /* uninit the context; new contexts could still be created */
	pup_uninit(&plugins); /* unload plugins before the final fungw atexit call, so plugin engines can unregister from fungw */
	fgw_atexit();         /* free all memory used by the lib */

	return 0;
}
