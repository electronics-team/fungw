#include <stdlib.h>
#include <math.h>
#include <libfungw/fungw.h>
#include <libfungwbind/c/fungw_c.h>

static fgw_error_t fgwc_sin(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	FGW_DECL_CTX;
	FGW_ARGC_REQ_MATCH(1);
	FGW_ARG_CONV(&argv[1], FGW_DOUBLE);
	res->val.nat_double = sin(argv[1].val.nat_double);
	res->type = FGW_DOUBLE;
	return FGW_SUCCESS;
}

static fgw_error_t fgwc_cos(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	FGW_DECL_CTX;
	FGW_ARGC_REQ_MATCH(1);
	FGW_ARG_CONV(&argv[1], FGW_DOUBLE);
	res->val.nat_double = cos(argv[1].val.nat_double);
	res->type = FGW_DOUBLE;
	return FGW_SUCCESS;
}

static fgw_error_t fgwc_tan(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	FGW_DECL_CTX;
	FGW_ARGC_REQ_MATCH(1);
	FGW_ARG_CONV(&argv[1], FGW_DOUBLE);
	res->val.nat_double = tan(argv[1].val.nat_double);
	res->type = FGW_DOUBLE;
	return FGW_SUCCESS;
}


/* Called when an object instance is created from this engine. This engine
   does not have any internal state, just pure math functions: just
   register those functions in the object being created. */
static int on_load(fgw_obj_t *obj, const char *filename, const char *opts)
{
	fgw_func_reg(obj, "my_sin",   fgwc_sin);
	fgw_func_reg(obj, "my_cos",   fgwc_cos);
	fgw_func_reg(obj, "my_tan",   fgwc_tan);
	return 0;
}

static const fgw_eng_t fgw_math_eng = {
	"math",                    /* name of the engine; third arg for fgw_obj_new() */
	fgws_c_call_script,        /* this C function is called when an engine
	                              function is called from the outside;
	                              fgws_c_call_script just calls a C function
	                              by pointer (stored on fgw_func_reg() calls) */
	NULL,                      /* init(), used in script engines so fungw can
	                              register some "system" functions in an empty
	                              script context before really loading the script */
	on_load                    /* called when an object instance is created from
	                              this engine */
};

void eng_math_init(void)
{
	fgw_eng_reg(&fgw_math_eng);
}
