/* The only public function of an engline is its initialization; once this
   is called, before an fgw_atexit() call, the engine is known to fungw
   and is available for instantiation in any context. */
void eng_math_init(void);

