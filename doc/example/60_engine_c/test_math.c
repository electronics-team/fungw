#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <libfungw/fungw.h>

#include "eng_math.h"

/* Import fawk init; this relies on two things:
   - fawk is always available in fungw, no external dep (well, the user can
     disable it in ./configure that'd lead to link error here)
   - fawk is in the big static lib (.a) of all languages and if we call its
     init, all relevant objects will get linked
*/
extern int pplg_init_fungw_fawk(void);

/* Handle runtime fungw errors - inform the user */
void async_error(fgw_obj_t *obj, const char *msg)
{
	printf("Async error: %s: %s\n", obj->name, msg);
}

/* Script calls C function: wrapper for atoi() */
fgw_error_t fgwc_atoi(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	FGW_DECL_CTX;                        /* setup for the FGW macro shorthands, initializes fgw_ctx_t *ctx */
	FGW_ARGC_REQ_MATCH(1);               /* require exactly one argument */
	FGW_ARG_CONV(&argv[1], FGW_STR);     /* make sure argv[1] is a string */

	/* call the function, prepare the result in res */
	res->val.nat_int = atoi(argv[1].val.str);
	res->type = FGW_INT;
	return FGW_SUCCESS;
}

int main(int argc, char *argv[])
{
	fgw_ctx_t ctx;
	fgw_obj_t *mobj, *sobj;

	/* Initialize and register the script language engine fawk */
	pplg_init_fungw_fawk();

	/* Initialize and register the math engine */
	eng_math_init();

	/* initialize a context */
	fgw_init(&ctx, "my_context");
	ctx.async_error = async_error;

	/* create an object called trigonometry, an instance of the math engine, with
	   no instance argument (our local math engine is not configurable) */
	mobj = fgw_obj_new(&ctx, "trigonometry", "math", NULL, NULL);
	assert(mobj != NULL);

	/* create objects for the script: loads script and runs its main() */
	sobj = fgw_obj_new(&ctx, "test_script", "fawk", "test_math.fawk", NULL);
	assert(sobj != NULL);

	fgw_uninit(&ctx);   /* uninit the context; new contexts could still be created */
	fgw_atexit();       /* free all memory used by the lib */

	return 0;
}
