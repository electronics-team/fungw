Example engine written in C, implementing a simple math lib with my_sin(),
my_cos() and my_tan(). The app registers the math lib engine and loads
and runs an example fawk script that calls these functions.

Build considerations: static linking, as explained in example 10_script.
