Demonstrates different ways to call a fungw registered (potentially
script) function from C.

Build considerations: static linking, as explained in example 10_script.
