Demonstrates how an app can register custom data types that then
can be used as arguments and return values in fungw registered functions.

Script bindings and stock engines know the standard "base" fungw types only.
The app can register a new data type, FGW_MM for millimeter in our example,
and provide converters between the new type and the base types. This lets
the app deal with native data types, having the conversion coordinated
by fungw.

There are two implementations shown:

- The lazy implementation has only a string <-> FGW_MM converter and lets
  fungw do a second conversion between the string and whatever base type
  that was requested. This is less code but runs slower. Can not write
  unit when converting to string because in many cases fungw will try to
  convert the string further to a number where the unit would cause an error.

- The proper implementation attempts to convert between FGW_MM and any base
  type where the conversion makes sense. Which is all numeric types and
  string. This variant can write unit in string target as it is not
  used as an intermediate format.

