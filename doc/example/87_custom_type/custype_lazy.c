#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <assert.h>
#include <libfungw/fungw.h>
#include <libfungwbind/c/fungw_c.h>
#include <libfungw/fungw_conv.h>

/* millimeter type ID; store distance in mm in the ->long field */
fgw_type_t FGW_MM;

/* Convert from numeric to string without unit (see later) */
static char *mm_dup_str(long val)
{
	char *res = malloc(32);
	sprintf(res, "%ld", val);
	return res;
}

/* Convert (parse) a string, potentially user supplied, into mm */
static long mmtol(const char *s, const char **end)
{
	char *e;
	long res = strtol(s, &e, 10);
	if (*e != '\0') {
		while(isspace(*e)) e++;
		if (strcmp(e, "mm") == 0) { *end = e+2; }
		else if (strcmp(e, "km") == 0) { *end = e+2; res *= 1000l*1000l; }
		else if (strcmp(e, "m") == 0)  { *end = e+1; res *= 1000; }
		else if (strcmp(e, "dm") == 0) { *end = e+2; res *= 100; }
		else if (strcmp(e, "cm") == 0) { *end = e+2; res *= 10; }
		else *end = e;
	}
	else
		*end = e;
	return res;
}


/* Lazy approach: convert only to/from string */
int mm_arg_conv_lazy(fgw_ctx_t *ctx, fgw_arg_t *arg, fgw_type_t target)
{
	if (target == FGW_MM) { /* convert from anything to mm */
		long tmp;
		const char *end;

		/* first ask fungw convert to string... */
		if (fgw_arg_conv(ctx, arg, FGW_STR) != 0) 
			return -1;

		/* ... then parse the string (it will have no unit if input was numeric) */
		tmp = mmtol(arg->val.str, &end);
		if (arg->type & FGW_DYN)
			free(arg->val.str);
		if (*end != '\0')
			return -1;
		arg->type = FGW_MM;
		arg->val.nat_long = tmp;
		return 0;
	}
	if (arg->type == FGW_MM) { /* convert from mm to anything */
		long tmp = arg->val.nat_long;

		/* Cheat: whatever type fungw asked for, just return string; all we are
		   required to do is return a base (non-custom) fungw type. In case
		   the type we return here doesn't match the type fungw requested, it
		   will automatically do a base->base type conversion. For example:
		   fungw wanted double; we return string; fung will do a string->double
		   conversion. This way we do not need to handle each type or type group
		   separately -> less code, worse runtimes.

		   Note: we can't print unit here, because if target type is not string,
		   the conversion would fail. E.g. if we return "15 mm" and fungw wanted
		   a double, it will do an strtod() which will fail at the first 'm'.
		 */

		arg->val.str = mm_dup_str(tmp);
		arg->type = FGW_STR | FGW_DYN;
		return 0;
	}

	/* if this function is ever called with neither side being an mm type,
	   that's a bug in libfungw */
	fprintf(stderr, "Neither side of the conversion is mm\n");
	abort();
}

void test(fgw_ctx_t *ctx)
{
	fgw_arg_t a, b;
	int res;

	a.val.str = "124cm";
	a.type = FGW_STR;

	res = fgw_arg_conv(ctx, &a, FGW_MM);
	printf("str to mm: %d: 0x%x %ld\n", res, a.type, a.val.nat_long);

	b = a;

	res = fgw_arg_conv(ctx, &a, FGW_STR);
	printf("mm to str: %d: 0x%x %s\n", res, a.type, a.val.str);

	res = fgw_arg_conv(ctx, &b, FGW_DOUBLE);
	printf("mm to double: %d: 0x%x %f\n", res, b.type, b.val.nat_double);
}

int main(int argc, char *argv[])
{
	fgw_ctx_t ctx;

	fgw_init(&ctx, "host");

	FGW_MM = fgw_reg_custom_type(&ctx, 0, "mm", mm_arg_conv_lazy, NULL);
	assert(FGW_MM != FGW_INVALID);

	test(&ctx);

	fgw_uninit(&ctx);
	fgw_atexit();
	return 0;
}
