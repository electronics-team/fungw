#include <stdio.h>
#include <assert.h>
#include <libfungw/fungw.h>

/* Two "event handler" functions that will be bound to the same function
   name from two different objects */
fgw_error_t event_cb1(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	FGW_DECL_CTX;
	printf(" event_cb1: '%s'\n", argv[1].val.str);
	return FGW_SUCCESS;
}

fgw_error_t event_cb2(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	FGW_DECL_CTX;
	printf(" event_cb2: '%s'\n", argv[1].val.str);
	return FGW_SUCCESS;
}


int main(int argc, char *argv[])
{
	fgw_ctx_t ctx;
	fgw_obj_t *obj1, *obj2;
	fgw_arg_t res;

	fgw_init(&ctx, "host");

	/* create two objects, emulating e.g. two scripts in different languages */
	obj1 = fgw_obj_reg(&ctx, "o1");
	obj2 = fgw_obj_reg(&ctx, "o2");

	/* register obj1.event to event_cb1 and obj2.event to event_cb2; they have
	   the same name, so there will be only one prefix-less event() in ctx */
	fgw_func_reg(obj1, "event", event_cb1);
	fgw_func_reg(obj2, "event", event_cb2);

	/* call event() directly, as a plain function: only obj1.event
	   handler will run (it was the first registered with the name "event"),
	   but we have a return value in res */
	printf("two objs, fgw_vcall:\n");
	fgw_vcall(&ctx, &res, "event", FGW_STR, "single", 0);

	/* call every event() from context; this means *.event() is called with
	   the same argument. (If a callee decides to convert the argument, that
	   affects only that single call, the other calls will still get the
	   argument with the original type). There is no &res return value as
	   each function could return something different. */
	printf("two objs, fgw_vcall_all:\n");
	fgw_vcall_all(&ctx, "event", FGW_STR, "multi", 0);

	/* free all resources */
	fgw_uninit(&ctx);
	fgw_atexit();
	return 0;
}
