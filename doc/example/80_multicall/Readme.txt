Demo on registering multiple functions under the same short name within
a context and using them as "event handlers" in a multi-call where
a single call is directed to each function with matching name.

A typical real-life application would be that different scripts and
engines would register their event handler function using the event name.
The application, upon generating the event, would do a multi-call to the
short name. Fungw then looks at each object in the context and see if the
object has a function with matching name; if so, it is called. Thus the
event is delivered to any object in the context that has an event handler
function with the required function name.

Limitations:

- order of calls within a multi-call is unspecified (it is _not_ the order
  of function registration or object creation)

- a multicall has no return value (each function call would have its own
  return value and the dispatcher wouldn't know which one to return to the
  caller)

- it is not possible to cancel a multi-call before every matching function is
  called (e.g. if one of the callees wants to announce "the event is handled")

